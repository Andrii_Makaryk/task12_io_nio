import java.io.Serializable;

public class Droid implements Serializable {
    int droidID;

    public Droid(int droidID) {
        this.droidID = droidID;
    }

    public int getDroidID() {
        return droidID;
    }

    public void setDroidID(int droidID) {
        this.droidID = droidID;
    }
}
